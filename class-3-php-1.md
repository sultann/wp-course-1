# Class-3 (PHP-1)

## What is PHP?
PHP (Hypertext Preprocessor) is a widely-used open source server side scripting language that is especially suited for web development and can be embedded into HTML.

PHP is a case sensitive language.

## What Can PHP Do?
* PHP can generate dynamic page content
* PHP can create, open, read, write, delete, and close files on the server
* PHP can collect form data
* PHP can send and receive cookies
* PHP can add, delete, modify data in your database
* PHP can output any kind of data. eg. text, HTML and XML

## WHy PHP
* PHP is open source
* PHP is compatible with almost all servers used today
* PHP is easy to learn and runs efficiently on the server side

## How it works?
* `<?php // PHP code goes here ?>`
* `<?php` Opening Tag
* `?>` Closing Tag
* File extension must be *.php


## Commenting

```php
// This is a single-line comment
```

```php
\# This is also a single-line comment
```

```php
/*
This is a multiple-lines comment block <br/>
that spans over multiple <br/>
lines <br/>
*/
```

```php
// You can also use comments to leave out parts of a code line
```


## Variable
a variable starts with the $ sign, followed by the name of the variable.
```php
<?php
 $txt = "Hello world!";
 $x = 5;
 $y = 10.5;
 ?>
```
 ## Rules to remember
 * A variable can have a short name (like x and y) or a more descriptive name (age, carname, total_volume).
 * A variable starts with the $ sign, followed by the name of the variable
 * A variable name must start with a letter or the underscore character
 * A variable name cannot start with a number
 * A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
 * Variable names are case-sensitive ($age and $AGE are two different variables)
 
 > Remember that PHP variable names are case-sensitive!
 
 ## echo and print
 
 
 ## Data types
 Variables can store data of different types, and different data types can do different things.
 
 PHP supports the following data types:
 * String
 * Integer
 * Float (floating point numbers - also called double)
 * Boolean
 * Array
 * Object
 * NULL

 
 ## String
 A string is a sequence of characters, like "Hello world!".
 
 ## Constant
 To create a constant, use the define() function.
 ### Syntax 
 ```php
define(name, value, case-insensitive)
```
#### Parameters:
* name: Specifies the name of the constant
* value: Specifies the value of the constant
* case-insensitive: Specifies whether the constant name should be case-insensitive. Default is false

### Example
```php
<?php
define("GREETING", "Welcome to W3Schools.com!");
echo GREETING;
?>
```
 
 ## Operators
 Operators are used to perform operations on variables and values.
 
 PHP divides the operators in the following groups:
 * Arithmetic operators
 * Assignment operators
 * Comparison operators
 * Increment/Decrement operators
 * Logical operators
 * String operators
 * Array operators

Check it in w3 school


 
 ##  if...else...elseif
 The if statement executes some code if one condition is true.
 
 ### if
 #### syntax
 ```php
if (condition) {
    code to be executed if condition is true;
}
```
#### Example
```php
<?php
$t = date("H");

if ($t < "20") {
    echo "Have a good day!";
}
?>
```

 ### if...else
 #### syntax
 ```php
if (condition) {
    code to be executed if condition is true;
} else {
    code to be executed if condition is false;
}
```
#### Example
```php
<?php
$t = date("H");

if ($t < "20") {
    echo "Have a good day!";
} else {
    echo "Have a good night!";
}
?>
```

### if...elseif....else
 #### syntax
 ```php
if (condition) {
    code to be executed if this condition is true;
} elseif (condition) {
    code to be executed if this condition is true;
} else {
    code to be executed if all conditions are false;
}
```
#### Example
```php
<?php
$t = date("H");

if ($t < "10") {
    echo "Have a good morning!";
} elseif ($t < "20") {
    echo "Have a good day!";
} else {
    echo "Have a good night!";
}
?>
```
 
 
 ## switch Statement
 switch statement is used to select one of many blocks of code to be executed.
  #### Syntax
 ```php
switch (n) {
    case label1:
        code to be executed if n=label1;
        break;
    case label2:
        code to be executed if n=label2;
        break;
    case label3:
        code to be executed if n=label3;
        break;
    ...
    default:
        code to be executed if n is different from all labels;
}
```
 #### Example
```php
<?php
$favcolor = "red";

switch ($favcolor) {
    case "red":
        echo "Your favorite color is red!";
        break;
    case "blue":
        echo "Your favorite color is blue!";
        break;
    case "green":
        echo "Your favorite color is green!";
        break;
    default:
        echo "Your favorite color is neither red, blue, nor green!";
}
?>
```
 
 ## while Loops
The while loop executes a block of code as long as the specified condition is true.
 #### Example
 ```php
while (condition is true) {
    code to be executed;
}
```
#### Example
```php
while (condition is true) {
    code to be executed;
}
```
 
 ## for Loops
 for loops are used to execute a block of code for a specified number of times.
 #### Syntax
 for (init counter; test counter; increment counter) {
     code to be executed;
 }
  ##### Parameters:
 * init counter: Initialize the loop counter value
 * test counter: Evaluated for each loop iteration. If it evaluates to TRUE, the loop continues. If it evaluates to FALSE, the loop ends.
 * increment counter: Increases the loop counter value
 
 #### Example
 ```php
<?php 
for ($x = 0; $x <= 10; $x++) {
    echo "The number is: $x <br>";
} 
?>
```
## For each Loop
The foreach loop works only on arrays, and is used to loop through each key/value pair in an array.
#### Syntax
```php
foreach ($array as $value) {
    code to be executed;
}
```
#### Example
```php
<?php 
$colors = array("red", "green", "blue", "yellow"); 

foreach ($colors as $value) {
    echo "$value <br>";
}
?>
```
  
 ## Functions
  >A function is a block of statements that can be used repeatedly in a program.
  
  A function will be executed by a call to the function.
  #### Syntax
  ```php
function functionName() {
    code to be executed;
}
```
Note: A function name can start with a letter or underscore (not a number).

#### Example
```php
<?php
function writeMsg() {
    echo "Hello world!";
}

writeMsg(); // call the function
?>
```
## Arrays
While variable can hold one single value Array is just like pocket
it can contains multiple value at a time.
```php
array()
```
In PHP, there are three types of arrays:
* Indexed arrays - Arrays with a numeric index
* Associative arrays - Arrays with named keys
* Multidimensional arrays - Arrays containing one or more arrays

### Indexed Arrays
```php
$cars = array("Volvo", "BMW", "Toyota");
```

### Associative Arrays
```php
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
```
### Multidimensional Arrays
```php
$cars = array
  (
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15)
  );
```


  